const express = require('express'); //path
const cors = require('cors');
const mongoose = require ('mongoose');
const viewersRoutes = require('./routes/Viewer') ;// inaaccess yung path 
const app = express (); // from express import to access the express 

app.use(cors());

/*connect to mondogb atlas via mongoose*/
mongoose.connect('mongodb+srv://admin:Hachi2829@wdc028-course-booking.naodr.mongodb.net/dbBooking_app?retryWrites=true&w=majority',{
	useNewUrlParser: true,
	useUnifiedTopology: true,
	useFindAndModify: false
});


// set notification for connection sucess or failure
let db = mongoose.connection;
// if conenction error encountered, output it in console 
db.on('error', (console.error.bind(console, 'connection error: ')));

// once connection, show notification
db.once('open', () => console.log("We're connected to our database."))


// Middleware - built in function ng app 
app.use(express.json()) 
app.use(express.urlencoded( {extended:true}));

//define Schema - structure of documents
const taskSchema = new mongoose.Schema({
	name: String,
	status: {
		type: String,
		default: 'pending'
	}
})

const Task = mongoose.model('Task',taskSchema); 
//MVC- model(blueprints),views(userinterface),controllers (logic)
//naka acapital yung task bc model siya 
/* Create a user schema using monggose with the following fields
	username: string
	email: string
	password:
	//tasl is an array of embbeded task documents, it refers to taskSchema 
	tasks:array
*/

const userSchema = new mongoose.Schema({
	username: String,
	email: String,
	password: String,
	task: [taskSchema]
})

const User = mongoose.model('User',userSchema); //nagcocompile 
/*
//request 
<body>
	<form>
		<input name="username" value="Thonie">
		<input name="email" value="Thonie@yahoo.com">
	</form>
</body>

app.get('/',(req,res)=>{
	{
		username:req.body.username
	}
	{
		email: req.body.email
	}
})
*/



app.post('/user', (req, res)=> { 
	//check if username or email are duplicate prior to registration
	User.find({ $or: [{ username: req.body.username }, { email: req.body.email }] }, (findErr, duplicates) => {
			if(findErr) return console.error(findErr);
			//if duplicates found
			if(duplicates.length > 0) {
				return res.status(403).json({
					message: "Duplicates found, kindly choose different username and/or email."
				})

			} else {
			//instantiate a new user object with properties derived from the request body
			let newUser = new User({
				username: req.body.username,
				email: req.body.email,
				password: req.body.password,
				//tasks is initially an empty array
				tasks: []
			})
			//save
			newUser.save((saveErr, newUser) => {
				//if an error was ecncounter while saving the document - notification in the console
				if(saveErr) return console.error(saveErr);
				return res.status(201).json({
						message: `User ${newUser.username} successfull registered,`,
						data: {
							username: newUser.username,
							email: newUser.email,
							link_to_self: `/user/${newUser._id}`
						}
				});
			})
		}
	})	
});

// display user 
app.get('/user/:id',(req,res) => {
	User.findById(req.params.id , (err, user) =>{
		if(err) return console.error(err);
		return res.status(200).json({
			message: "User retrieved sucessfully.",
			data: { 
				username: user.username,
				email: user.email,
				tasks: `/user/${user._id}/task`
			}
		})
	})

})











/* Routes*/
app.use('/',viewersRoutes); // user naglalagay nung /viewers 





// create port - communication - 3000
const port = 3000;

app.listen(port,() => {console.log(`Listening on port ${port}.`)}) 